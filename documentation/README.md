## External Documents

* [ACPI Specification](https://uefi.org/sites/default/files/resources/ACPI_6_3_final_Jan30.pdf)
* [AHCI Specification](https://www.intel.com/content/dam/www/public/us/en/documents/technical-specifications/serial-ata-ahci-spec-rev1-3-1.pdf)
* [ATAPI specification](http://www.t13.org/Documents/UploadedDocuments/docs2016/di529r14-ATAATAPI_Command_Set_-_4.pdf)
* [SATA Specification](http://www.lttconn.com/res/lttconn/pdres/201005/20100521170123066.pdf)
* [UEFI Specification](https://uefi.org/sites/default/files/resources/UEFI_Spec_2_8_final.pdf)
* [X86_64 Architecture](https://www.amd.com/system/files/TechDocs/24593.pdf)
* [X86_64 Opcode Manual](https://www.amd.com/system/files/TechDocs/24594.pdf)
* [X86_64 Opcode Reference](http://ref.x86asm.net/coder64.html)
